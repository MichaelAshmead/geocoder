import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Runner {

	public static void main(String[] args) throws IOException, InterruptedException {
		BufferedReader in = new BufferedReader(new FileReader("colleges.txt"));
		BufferedWriter out = new BufferedWriter(new FileWriter("file.txt"));
		String strLine;
		try {
			while ((strLine = in.readLine()) != null)   {
				String string = GeocodeImplementation.getJSONByGoogle(strLine);
				if (string.indexOf("location") != -1) {
					string = string.substring(string.indexOf("location"));
					string = string.substring(string.indexOf("lat"));
					string = string.replace("lat","");
					string = string.replace("lng","");
					string = string.replace(" ","");
					string = string.replace("\n","");
					string = string.replace(":","");
					string = string.replace("\"","");
					string = string.substring(0,string.indexOf("}"));
					out.write(strLine + "," + string+"\n");
					System.out.println(strLine + "," + string);
				}
				else {
					out.write(strLine + "\n");
					System.out.println(strLine);
				}
				Thread.sleep(10000);
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}